from page_objects import PageObject, PageElement
class MainPage (PageObject):
    def check_page (self):
        return "Logged" in self.w.title

    logout_link = PageElement(link_text="Logout")

    def click_logout(self, welcomepage):
        self.logout_link.click()
        return welcomepage.check_page()
